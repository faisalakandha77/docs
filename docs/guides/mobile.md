---
id: mobile
title: Mobile
---

Minds uses [React Native](https://reactnative.com) to power both the Android and iOS applications. The source code can be found [here](https://gitlab.com/minds/mobile-native).

## Building

`yarn install`

`react-native run-ios or react-native run-android`

## Spec tests

### Executing

`yarn test`
