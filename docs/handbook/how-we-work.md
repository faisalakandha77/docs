---
id: how-we-work
title: How we work
---

Minds is a distributed team located on both sides of the Atlantic. We communicate via [Zulip](https://zulip.com), [Gitlab](https://gitlab.com) and Minds Gatherings (video chat).

## Squads

| Squad  | Members                     | Scrum Calls Meeting Times                  |
| ------ | --------------------------- | ------------------------------------------ |
| Blue   | Guy, Olivia, Rami           | Monday + Thursday 5PM UK / 12PM NY         |
| Green  | Brian, Juan, Martin, Xander | Tuesday + Thursdays 4.30PM UK / 11.30AM NY |
| Yellow | Ben, Emi, Marcelo           | Tuesday + Thursdays 1.30PM UK / 8.30AM NY  |

### Video Scrums

[Click here](https://www.minds.com/groups/profile/569521254306951168/feed) to join the calls.

Each squad has a video call twice per week. The squad answers the following questions:

1. What did you yesterday
2. What are you going to do today
3. Are there any issues

### Asynchronous Scrums

On days where the squad does not have a video call scheduled, the scrum will be performed offline and still follow the same 3 question patterns as noted above.

## Coffee Chats

Optional daily 30 minute meetings where team members can hop in and out, discuss issues or share knowledge. Time TBC.

## QA

Please visit the [REAMDE.md](https://gitlab.com/minds/qa) on [https://gitlab.com/minds/qa](https://gitlab.com/minds/qa) for more information about our QA process.

## Project management

_This section is still being constructed_.

### Issues and Epics

The full project management workflow board can be found [here](https://gitlab.com/groups/minds/-/boards/1264761).

[![Architecture diagram](assets/work-flow.png "Diagram of Minds Workflow")
Click to enlarge](assets/work-flow.png)

## Work in progress

This document is a work in progress. Let's discuss how to improve our processes!
